#pragma once

#include <string>

INT_PTR CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);

typedef struct {
	unsigned number;
	WPARAM selection;
	std::wstring dir;
} WndVar;
