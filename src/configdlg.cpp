#include "stdafx.h"
#include "configdlg.h"
#include "resource.h"

constexpr size_t bufSize{ 1024 };

inline static bool isValidInput(const WndVar* hWndVar) {
	const bool isNotNumber{ hWndVar && hWndVar->selection > IDC_SELCETION_MIN && hWndVar->selection < IDC_SELCETION_MAX && hWndVar->selection != IDC_NUMBER };
	const bool isValidNumber{ hWndVar && hWndVar->selection == IDC_NUMBER && hWndVar->number > 0 };
	return isNotNumber || isValidNumber;
}

INT_PTR CALLBACK DlgProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch (uMsg) {
	case WM_INITDIALOG:
	{
		SetWindowLongPtrW(hWnd, GWLP_USERDATA, static_cast<LONG_PTR>(lParam));
		auto hWndVar = reinterpret_cast<WndVar*>(lParam);
		SetDlgItemTextW(hWnd, IDC_TEXT_DIR, hWndVar->dir.c_str());
		EnableWindow(GetDlgItem(hWnd, IDC_NUMFILES), FALSE);
		EnableWindow(GetDlgItem(hWnd, IDC_STATIC_NUMFILES), FALSE);
		EnableWindow(GetDlgItem(hWnd, IDOK), FALSE);
		break;
	}

	case WM_CLOSE:
		EndDialog(hWnd, IDCANCEL);
		break;

	case WM_COMMAND:
	{
		auto hWndVar = reinterpret_cast<WndVar*>(GetWindowLongPtrW(hWnd, GWLP_USERDATA));

		switch (LOWORD(wParam)) {
		case IDC_NUMFILES:
			if (hWndVar) {
				char buf[bufSize];
				GetDlgItemTextA(hWnd, IDC_NUMFILES, buf, bufSize);
				if (1 != sscanf(buf, "%u", &hWndVar->number)) {
					hWndVar->number = 0;
				}
				EnableWindow(GetDlgItem(hWnd, IDOK), isValidInput(hWndVar));
			}
			break;

		case IDC_NUMBER:
		case IDC_EXTENSION:
		case IDC_DATE_YYYY:
		case IDC_DATE_YYYYMM:
		case IDC_DATE_YYYYMMDD:
			if (hWndVar) {
				hWndVar->selection = SendDlgItemMessageW(hWnd, LOWORD(wParam), BM_GETCHECK, 0, 0) ? LOWORD(wParam) : 0;
				SendDlgItemMessageW(hWnd, IDC_NUMBER, BM_SETCHECK, IDC_NUMBER == hWndVar->selection ? BST_CHECKED : BST_UNCHECKED, 0);
				SendDlgItemMessageW(hWnd, IDC_EXTENSION, BM_SETCHECK, IDC_EXTENSION == hWndVar->selection ? BST_CHECKED : BST_UNCHECKED, 0);
				SendDlgItemMessageW(hWnd, IDC_DATE_YYYY, BM_SETCHECK, IDC_DATE_YYYY == hWndVar->selection ? BST_CHECKED : BST_UNCHECKED, 0);
				SendDlgItemMessageW(hWnd, IDC_DATE_YYYYMM, BM_SETCHECK, IDC_DATE_YYYYMM == hWndVar->selection ? BST_CHECKED : BST_UNCHECKED, 0);
				SendDlgItemMessageW(hWnd, IDC_DATE_YYYYMMDD, BM_SETCHECK, IDC_DATE_YYYYMMDD == hWndVar->selection ? BST_CHECKED : BST_UNCHECKED, 0);
				EnableWindow(GetDlgItem(hWnd, IDC_NUMFILES), hWndVar && hWndVar->selection == IDC_NUMBER);
				EnableWindow(GetDlgItem(hWnd, IDC_STATIC_NUMFILES), hWndVar && hWndVar->selection == IDC_NUMBER);
				EnableWindow(GetDlgItem(hWnd, IDOK), isValidInput(hWndVar));
			}
			break;

		default:
			break;
		}

		if (wParam == IDOK) {
			if (isValidInput(hWndVar)) {
				EndDialog(hWnd, IDOK);
			}
		}
		else if (wParam == IDCANCEL) {
			EndDialog(hWnd, IDCANCEL);
		}
		break;
	}

	default:
		return FALSE;
	}

	return TRUE;
}
