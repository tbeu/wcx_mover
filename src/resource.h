#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define IDD_CONFIG                              100
#define IDC_STATIC_DIR                          40000
#define IDC_TEXT_DIR                            40001
#define IDC_STATIC_NUMFILES                     40002
#define IDC_NUMFILES                            40003
#define IDC_SELCETION_MIN                       40004
#define IDC_NUMBER                              40005
#define IDC_EXTENSION                           40006
#define IDC_DATE_YYYY                           40007
#define IDC_DATE_YYYYMM                         40008
#define IDC_DATE_YYYYMMDD                       40009
#define IDC_SELCETION_MAX                       40010
