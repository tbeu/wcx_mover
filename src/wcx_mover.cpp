// wcx_mover.cpp : Definiert die exportierten Funktionen für die DLL-Anwendung.
//

#include "stdafx.h"
#include "configdlg.h"
#include "cunicode.h"
#include "resource.h"
#include "wcxhead.h" // WCX
#include <filesystem>

namespace fs = std::filesystem;

inline static std::time_t GetLastWriteTime(const fs::path& filename) {
#if defined(_WIN32)
	if (struct _stat64 fileInfo; 0 == _wstati64(filename.wstring().c_str(), &fileInfo)) {
		return fileInfo.st_mtime;
	}
	throw std::runtime_error("Failed to get last write time.");
#else
	auto fsTime = std::filesystem::last_write_time(filename);
	return decltype (fsTime)::clock::to_time_t(fsTime);
#endif
}

static std::wstring GetSepDirName(const fs::path& fullPath, const WndVar* hWndVar, size_t count) {
	switch (hWndVar->selection) {
	case IDC_DATE_YYYY:
	case IDC_DATE_YYYYMM:
	case IDC_DATE_YYYYMMDD:
	{
		const auto cftime{ GetLastWriteTime(fullPath) };
		const auto ct{ std::localtime(&cftime) };
		wchar_t buf[16];
		if (IDC_DATE_YYYY == hWndVar->selection) {
			_snwprintf(buf, 15, L"%04i", ct->tm_year + 1900);
		}
		else if (IDC_DATE_YYYYMM == hWndVar->selection) {
			_snwprintf(buf, 15, L"%04i%02i", ct->tm_year + 1900, ct->tm_mon + 1);
		}
		else {
			_snwprintf(buf, 15, L"%04i%02i%02i", ct->tm_year + 1900, ct->tm_mon + 1, ct->tm_mday);
		}
		buf[15] = L'\0';
		return std::wstring(buf);
	}

	case IDC_EXTENSION:
		if (fullPath.has_extension()) {
			if (const auto ext{ fullPath.extension().wstring() }; ext.size() > 1) {
				return ext.substr(1);
			}
		}
		break;

	case IDC_NUMBER:
	{
		const size_t num { count / hWndVar->number + 1 };
		wchar_t buf[16];
		_snwprintf(buf, 15, L"%Iu", num);
		buf[15] = L'\0';
		return std::wstring(buf);
	}

	default:
		break;
	}

	return std::wstring();
}

// packer plugin function declarations
HANDLE __stdcall OpenArchive(tOpenArchiveData *ArchiveData);
HANDLE __stdcall OpenArchiveW(tOpenArchiveDataW *ArchiveData);
int __stdcall CloseArchive(HANDLE hArcData);
int __stdcall ReadHeader(HANDLE hArcData, tHeaderData *HeaderData);
int __stdcall ReadHeaderExW(HANDLE hArcData, tHeaderDataExW *HeaderData);
int __stdcall ProcessFile(HANDLE hArcData, int Operation, char *DestPath, char *DestName);
int __stdcall ProcessFileW(HANDLE hArcData, int Operation, wchar_t* DestPath, wchar_t* DestName);
int __stdcall GetPackerCaps(void);
void __stdcall SetChangeVolProc(HANDLE hArcData, tChangeVolProc pChangeVolProc1);
void __stdcall SetChangeVolProcW(HANDLE hArcData, tChangeVolProcW pChangeVolProc1);
void __stdcall SetProcessDataProc(HANDLE hArcData, tProcessDataProc pProcessDataProc1);
void __stdcall SetProcessDataProcW(HANDLE hArcData, tProcessDataProcW pProcessDataProc1);
void __stdcall ConfigurePacker(HWND Parent, HINSTANCE DllInstance);
void __stdcall PackSetDefaultParams(PackDefaultParamStruct* dps);
void __stdcall PackSetDefaultParamsW(PackDefaultParamStructW* dps);
int __stdcall DeleteFiles(char *PackedFile, char *DeleteList);
int __stdcall DeleteFilesW(wchar_t* PackedFile, wchar_t* DeleteList);
int __stdcall PackFiles(char *PackedFile, char *SubPath, char *SrcPath, char *AddList, int Flags);
int __stdcall PackFilesW(wchar_t* PackedFile, wchar_t* SubPath, wchar_t* SrcPath, wchar_t* AddList, int Flags);

HINSTANCE hinst = nullptr;
static tProcessDataProc pProcessDataProc;
static tChangeVolProc pChangeVolProc;
static tProcessDataProcW pProcessDataProcW;
static tChangeVolProcW pChangeVolProcW;

HANDLE __stdcall OpenArchive(tOpenArchiveData *ArchiveData) {
	ArchiveData->OpenResult = E_UNKNOWN_FORMAT;
	return nullptr;
}

HANDLE __stdcall OpenArchiveW(tOpenArchiveDataW *ArchiveData) {
	ArchiveData->OpenResult = E_UNKNOWN_FORMAT;
	return nullptr;
}

int __stdcall CloseArchive(HANDLE hArcData) {
	return 0;
}

int __stdcall ReadHeader(HANDLE hArcData, tHeaderData *HeaderData) {
	return E_BAD_ARCHIVE;
}

int __stdcall ReadHeaderExW(HANDLE hArcData, tHeaderDataExW *HeaderData) {
	return E_BAD_ARCHIVE;
}

int __stdcall ProcessFile(HANDLE hArcData, int Operation, char *DestPath, char *DestName) {
	return E_BAD_ARCHIVE;
}

int __stdcall ProcessFileW(HANDLE hArcData, int Operation, wchar_t* DestPath, wchar_t* DestName) {
	return E_BAD_ARCHIVE;
}

int __stdcall DeleteFiles(char *PackedFile, char *DeleteList) {
	return E_NOT_SUPPORTED;
}

int __stdcall DeleteFilesW(wchar_t* PackedFile, wchar_t* DeleteList) {
	return E_NOT_SUPPORTED;
}

int __stdcall GetPackerCaps(void) {
	return PK_CAPS_NEW | PK_CAPS_MULTIPLE | PK_CAPS_MODIFY | PK_CAPS_HIDE;
}

void __stdcall SetChangeVolProc(HANDLE hArcData, tChangeVolProc pChangeVolProc1) {
	pChangeVolProc = pChangeVolProc1;
}

void __stdcall SetChangeVolProcW(HANDLE hArcData, tChangeVolProcW pChangeVolProc1) {
	pChangeVolProcW = pChangeVolProc1;
}

void __stdcall SetProcessDataProc(HANDLE hArcData, tProcessDataProc pProcessDataProc1) {
	pProcessDataProc = pProcessDataProc1;
}

void __stdcall SetProcessDataProcW(HANDLE hArcData, tProcessDataProcW pProcessDataProc1) {
	pProcessDataProcW = pProcessDataProc1;
}

int __stdcall PackFiles(char *PackedFile, char *SubPath, char *SrcPath, char *AddList, int Flags) {
	char* AddList2 = AddList;
	while (*AddList) {
		AddList += strlen(AddList) + 1;
	}
	size_t s = AddList - AddList2 + 1;
	wchar_t* AddListW = new wchar_t[s];
	ZeroMemory(AddListW, s*(sizeof(wchar_t)));
	AddList = AddList2;
	wchar_t* AddListW2 = AddListW;
	while (*AddList) {
		awlcopy(AddListW, AddList, (int)strlen(AddList) + 1);
		AddListW += wcslen(AddListW) + 1;
		AddList += strlen(AddList) + 1;
	}

	wchar_t PackedFileW[wdirtypemax], SubPathW[wdirtypemax], SrcPathW[wdirtypemax];
	ZeroMemory(PackedFileW, wdirtypemax*(sizeof(wchar_t)));
	ZeroMemory(SubPathW, wdirtypemax*(sizeof(wchar_t)));
	ZeroMemory(SrcPathW, wdirtypemax*(sizeof(wchar_t)));

	int retVal = PackFilesW(awfilenamecopy(PackedFileW, PackedFile), awfilenamecopy(SubPathW, SubPath), awfilenamecopy(SrcPathW, SrcPath), AddListW2, Flags);
	delete[] AddListW;
	return retVal;
}

int __stdcall PackFilesW(wchar_t* PackedFile, wchar_t* SubPath, wchar_t* SrcPath, wchar_t* AddList, int Flags) {
	int retVal = 0;

	if (nullptr != SubPath || nullptr == SrcPath) {
		return E_EWRITE;
	}

	if (AddList) {
		fs::path targetDir{ PackedFile };
		targetDir.remove_filename();

		WndVar wndVar{ 0, 1, targetDir.wstring() };
		if (const auto rc{ DialogBoxParamW(hinst, MAKEINTRESOURCE(IDD_CONFIG), GetActiveWindow(), DlgProc, reinterpret_cast<LPARAM>(&wndVar)) }; IDOK != rc) {
			return E_EABORTED;
		}

		size_t count{ 0 };

		// process AddList
		do {
			// get file fullPath
			fs::path fullPath{ SrcPath };
			try {
				fs::path fileName{ AddList };
				if (!fileName.is_absolute()) {
					fullPath /= fileName;
				}
				else {
					fullPath = std::move(fileName);
					throw std::runtime_error("Unsupported use case.");
				}

				auto fSize{ fs::file_size(fullPath) };

				// move file
				if (!fs::is_directory(fullPath)) {
					if (const auto sep{ GetSepDirName(fullPath, &wndVar, count++) }; !sep.empty()) {
						const fs::path targetPath{ targetDir / sep / fileName };
						fs::path createDir{ targetPath };
						createDir.remove_filename();
						fs::create_directories(createDir);
						// never overwrite
						if (!fs::exists(targetPath)) {
							fs::rename(fullPath, targetPath);
						}
					}
				}

				// report progress
				if (pProcessDataProcW) {
					if (fSize > static_cast<uintmax_t>(INT32_MAX)) {
						const auto sCount{ static_cast<uint32_t>(fSize) / INT32_MAX };
						for (uint32_t i{ 0 }; i < sCount; ++i) {
							pProcessDataProcW(fullPath.wstring().c_str(), INT32_MAX);
						}
						fSize %= INT32_MAX;
					}
					if (0 == pProcessDataProcW(fullPath.wstring().c_str(), static_cast<int>(fSize))) {
						retVal = E_EABORTED;
						break;
					}
				}
			}
			catch (std::exception& e) {
				const std::string msg{ e.what() };
				std::wstring wmsg;
				std::copy(msg.begin(), msg.end(), std::back_inserter(wmsg));
				MessageBoxT(nullptr, wmsg.c_str(), fullPath.wstring().c_str(), MB_OK | MB_ICONERROR);
				retVal = E_EWRITE;
				break;
			}

			AddList += wcslen(AddList) + 1;
		} while (*AddList);
	}

	return retVal;
}
