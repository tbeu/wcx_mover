Mover plugin 2.0.0.2 for Total Commander
========================================

 * License:
-----------

This software is released as freeware.


 * Disclaimer:
--------------

This software is provided "AS IS" without any warranty, either expressed or
implied, including, but not limited to, the implied warranties of
merchantability and fitness for a particular purpose. The author will not be
liable for any special, incidental, consequential or indirect damages due to
loss of data or any other reason.


 * Installation:
----------------

Open the plugin archive using Total Commander and installation will start.


 * Description:
---------------

Mover is a packer plugin for Total Commander to move and separate files (also
recursively in sub-directories) by number, extension/type or a customizable date
format. If the source list of files contains sub-directories, these sub-directories
will be recreated in the target directory to maintain the original directory
structure.

Choose between the following three options to separate the files.
 - Separate by number:
   Specify the number of files each newly created directory should contain. The
   last directory might contain less files in number if the total number of
   files is not an integer multiple of the specified number of files. No leading
   zeros are added to the directory names.

 - Separate by extension:
   For each file type an extra directory consisting of the file extension will
   be created in the target directory. Files without an extension will not be moved
   at all.

 - Separate by date:
   Choose between three date formats to separate the files depending on their
   last write date.
   - YYYY: Four-digit year
   - YYYYMM: Four-digit year followed by two-digit month in the range [1, 12]
   - YYYYMMDD: Additionally consists of the two-digit day in the range [1, 31]


 * ChangeLog:
-------------

 o Version 2.0.0.2 (07.07.2019)
   - minor code improvements
 o Version 2.0.0.1 (01.07.2019)
   - reimplemented


 * References:
--------------

 o WCX Writer's Reference by Christian Ghisler & Jiri Barton
   - http://ghisler.fileburst.com/plugins/wcx_ref2.21se.zip


 * Trademark and Copyright Statements:
--------------------------------------

 o Total Commander is Copyright � 1993-2019 by Christian Ghisler, Ghisler Software GmbH.
   - http://www.ghisler.com


 * Feedback:
------------

If you have problems, questions, suggestions please contact Thomas Beutlich.
 o Email: support@tbeu.de
 o URL: http://tbeu.totalcmd.net